import {
  Component as _Component,
  CSSProperties as _CSSProperties,
  MouseEvent as _MouseEvent,
  ReactNode as _ReactNode
} from 'react'

export const React: {
  Component: typeof _Component
  CSSProperties: _CSSProperties
  MouseEvent: _MouseEvent
  ReactNode: _ReactNode
} = (global as any).React

export type Style = React.CSSProperties

export type Children = React.ReactNode

interface IChildrenProp {
  children?: Children
}

export const component = {
  render: (renderer: () => JSX.Element) => renderer,

  children: {
    render: (renderer: (children: Children) => JSX.Element) => ({
      children
    }: IChildrenProp) => renderer(children),

    props: <TProps extends object>(defaultProps?: Partial<TProps>) => ({
      get render() {
        return (renderer: (props: TProps & IChildrenProp) => JSX.Element) => (
          props: TProps & IChildrenProp
        ) => {
          const combinedProps: TProps = {
            ...(defaultProps as object),
            ...(props as object)
          } as any

          return renderer(combinedProps)
        }
      }
    })
  },

  props: <TProps extends object>(defaultProps?: Partial<TProps>) => ({
    get render() {
      return (renderer: (props: TProps) => JSX.Element) => (props: TProps) => {
        const combinedProps: TProps = {
          ...(defaultProps as object),
          ...(props as object)
        } as any

        return renderer(combinedProps)
      }
    }
  })
}
